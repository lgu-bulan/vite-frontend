import React from "react";
import ArticleCard from "../components/ArticleCard/articlePosts";
import GovServices from "../components/GovServices/GovServices";
// import GotoTop from "../components/GtoTop/ButtonTop";
import MunicipalOverview from "../components/MunicipalOverview/MunicipalOveriew";
import Counter from "../components/MunicipalOverview/CounterMO";
import Jumbotron from "../components/Jumbotron/Jumbotron"
import Minfo from "../components/MInfo/MayorInfo"
import BarangayHistory from "../pages/SubPages/History-Town/Barangay/HistoryBarangay"

// TEST

function Home() {
  return (
    <>
      <Jumbotron />
      <Minfo />
      <MunicipalOverview />
      <Counter />
      <ArticleCard />
      <GovServices />
      <GotoTop />

    </>
  );
}

export default Home;
