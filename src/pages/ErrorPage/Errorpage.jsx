import React, { useEffect } from "react";
// import { useNavigate } from "react-router-dom";
import TestAppNavbar from "../../components/Navbar/NavBarTest";
import "./errorpage.css";

function Errorpage({ isAuthenticated }) {
  // const navigate = useNavigate();
  useEffect(() => {
    // set the background color of the body element
    document.body.style.backgroundColor = "#416475";
    document.body.style.marginBottom = "50px";
    return () => {
      document.body.style.backgroundColor = null;
      document.body.style.marginBottom = null;
    };
  }, []);

  return (
    <>
      {!isAuthenticated ? <></> : <TestAppNavbar />}
      <div className="div-error-page-container">
        <section className="error-container">
          <span>4</span>
          <span>
            <span className="screen-reader-text">0</span>
          </span>
          <span>4</span>
        </section>
        <h1 className="error-page-title-h1"> Error Page</h1>
        <p className="zoom-area">
          <b>Ooops..How did you end up here.</b>
        </p>
        <div className="link-container">
          {/* <a target="_blank" href="https://www.silocreativo.com/en/creative-examples-404-error-css/" className="more-link">Return to Homepage</a> */}
          <a
            className="more-link"
            href="/"
            // onClick={(e) => {
            //   e.preventDefault();
            //   localStorage.clear();
            //   // localStorage.setItem("user", null);
            //   // localStorage.setItem("AuthenticationStatus", false);
            //   navigate("/");
            // }}
          >
            Return to Homepage
          </a>
        </div>
      </div>
    </>
  );
}

export default Errorpage;
