import { Table } from "antd";
import React from "react";
import TimeLineData from "./BarangayTimeLineData.json";
function BarangayTimeLine() {
  const columns = [
    {
      title: "id#",
      dataIndex: "id",
      key: "id",
      className: "table-cell-bordered",
      width: 70,
      align:'center',

    },
    {
      title: "Barangay Name",
      dataIndex: "barangayName",
      key: "barangayName",
      className: "table-cell-bordered",
      width: 250,
    },
    {
      title: "Year Founded",
      dataIndex: "YearFounded",
      key: "YearFounded",
      className: "table-cell-bordered",
      width: 250,
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        dataSource={TimeLineData}
        pagination={{
          position: ["bottomCenter"],
        }}
        rowKey="id"
      />
    </div>
  );
}

export default BarangayTimeLine;
