import React from "react";
import PageBanner from "../../../components/Banner/PageBanner";
import "./HistoryTown.scss";
function HistoryTown() {
  return (
    <div className="Town-History-Container">
      <PageBanner title="Town History" />
      <div className="HistoryTown-div-content">
        <div className="First-Desc">
          <h1>Brief History of Bulan</h1>
          <small>By: Antonio Gilana</small>

          <h3>
            The town of Bulan has a colorful and dramatic history which dates
            back to the pre-Hispanic period.
          </h3>

          <h3>HOW BULAN GOT ITS NAME: THE LEGEND</h3>

          <p>
            The name “Bulan” went through several transformations. In the local
            Bulaneno dialect, it has several meanings. It may mean the month of
            the year, or the moon, or a luminary. According to a local
            historian, the great Valerio Zuñiga, the last meaning, taken in its
            intrinsic sense, is the more accepted term, due to the historical
            fact narrated by the “mga gurangan”, the old inhabitants of the
            town. They said that one night, a Spanish scouting expedition coming
            from the old seaport of Ticao sailing northward and at the level of
            the seacoast of Otabi, saw a big and beautiful luminary, the moon,
            from the right side of their ship. Some of the natives who
            accompanied the Spanish explorers as guides and crew members, showed
            happiness and excitement when they saw the moon which had caused
            their happiness. In memory of this happening, the Spaniards named
            the place, “Bulan” when they landed. Another version goes that one
            moonlit night, two fishermen ashore were apportioning their catch
            when the Spaniards who landed in the place approached them for
            information about the place. Thinking that they were being asked
            about the full moon rising in the east, they answered, “Bulan”.
            Historical records however show that our place was first identified
            as “Bililan”, then later on as “Builan”, ad then more later,
            “Bulan”.
          </p>
        </div>

        <div className="Second-Desc">
          <h3>PRE-SPANISH PERIOD</h3>

          <p>
            Archeological evidences point out that long before the coming of the
            Spaniards, the coasts of Sorsogon were already thriving with
            communities and settlers dating back to as early as 4,000 B.C., when
            the Indonesians reached Southern Luzon. The archeological findings
            excavated in San Juan, Magsaysay and Gate, which were evaluated to
            belong to the Ming and Sung dynasty support the theories of
            historical researchers that the southernmost tip of Luzon, mentioned
            by Beyer and other historians, probably including Bulan, showed
            signs of civilization as far as 960 A.D. Golden crowns, believed of
            exist from 91 B.C. to 79 A.D., were also excavated in Bulan.
          </p>
        </div>

        <div className="Third-Desc">
          <h3>SPANISH PERIOD: Early Periods</h3>
          <p>
            Historical records disclose that in 1569, an expedition led by
            Captain Luis Enriquez de Guzman and Fray Alonzo Jimenez, an
            Augustinian Friar, reached Sorsogon soil and found a small
            settlement of natives engaged in fishing and farming. This
            settlement was believed to be Otavi. It was in Otavi where Fr.
            Jimenez, together with Fr. Juan Orta, celebrated the first Mass in
            Luzon.
          </p>

          <p>
            On May 16,1572, Capitan-General Miguel Lopez de Legazpi divided what
            is now Sorsogon province into various encomiendas, and he allotted
            “Bililan”(Bulan) as a royal encomienda, which together with
            “Uban”(Juban) has a population of 280 or 70 whole tributes
          </p>

          <p>
            In 1583, the Franciscans began their evangelical work in Sorsogon.
            Subsequently in 1646, the Franciscans formed Gate as a visita of
            Bulusan. In January 1690, “Builan” was constituted as a pueblo civil
            and Gate was chosen as the town site. Fray Diego de Yepes
            assumedadministration of the town and at the same time its parish
            priest. He left Bulan sometime in 1696.
          </p>

          <p>
            The growth of Bulan as a town, however, would be arrested as it
            began to suffer from the pressures of intense Moro raids in
            Sorsogonwhich lasted up to the middle of the 19th century. In 1746,
            a very devastating Moro attack destroyed Gate, which was 12
            kilometers distant from the coast. Bulan was plundered and razed to
            the ground. Scores of natives were killed and injured. Women and
            children were taken as captives. Those who were able to survive
            escaped the town, fleeing to the hills and hinterlands, totally
            abandoning the town. For the next 55 years Bulan was erased from the
            maps.
          </p>
        </div>

        {/* <div className="4th-pic">
          <img
          src=""
          />
        </div> */}
      </div>
    </div>
  );
}

export default HistoryTown;
