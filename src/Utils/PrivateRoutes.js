import React, { useContext, useEffect, useState } from 'react';
import { Outlet, Navigate } from 'react-router-dom'
import { AuthContext } from "../../src/Context/authContext";
const PrivateRoutes = () => {

    // const { isAuthenticated } = useContext(AuthContext);
    const { isAuthenticated, setIsAuthenticated } = useContext(AuthContext);
    // // let auth = {'token':false}
    let auth = isAuthenticated
    // const [loading, setLoading] = useState(true);

    useEffect(() => {
        const storedAuth = localStorage.getItem('user');
        if (storedAuth) {
            setIsAuthenticated(true);
        }
        // setLoading(false);
    }, []);
    return (
        isAuthenticated ? <Outlet /> : <Navigate to="/sidebar" />
        // auth ? <Outlet /> : <Navigate to="/login" />
    )
}

export default PrivateRoutes