import React from "react";
import axios from "axios";
import { Card, Col, Row } from "react-bootstrap";
function ArticleCard() {

  const fetchData = async () => {
    try {
      const res = await axios
        .get("http://localhost:5000/api/all")
        .then((response) => {
          setPosts(response.data)
          console.log(posts) 
        });
      console.log(posts);
    } catch (error) {
      console.log(error);
    }
  };
  
  useEffect(() => {
    
    fetchData();

    const arrayArticles = posts?.map((data) => {
      return (
        <Col key={data.id}>
          <Card className="Card--Article shadow-sm">
            <Card.Img
              className="Img__ArticleCard"
              variant="top"
              alt="Card image"
              src={data.img}
            />
            <Card.Body className="Card--BodyArticle">
              {data.category === "PESO UPDATES" ? (
                <div className="div__tag mb-3">
                  <span className="tag tag-teal">{data.category}</span>
                </div>
              ) : data.category === "LATEST NEWS" ? (
                <div className="div__tag mb-3">
                  <span className="tag tag-purple">{data.category}</span>
                </div>
              ) : data.category === "MUNICIPAL NEWS" ? (
                <div className="div__tag mb-3">
                  <span className="tag tag-pink">{data.category}</span>
                </div>
              ) : (
                <></>
              )}
              <Card.Title>{data.title}</Card.Title>
              <Card.Text className="pArticle">{data.description}</Card.Text>
              <div className="div_User">
                <img
                  className="Img__CardAuthor"
                  // variant="top"
                  alt="authimg"
                  src={''}
                />
                <div className="div__userInfo">
                  <h5 style={{ margin: 0 }}>{data.Author}</h5>
                  <small>{data.date_Posted}</small>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      );
    }); //END MAP

    // Limits the showed result of map array by slicing 0 & 4 range
    let arraySlice = arrayArticles.slice(0, 4);

    setArticlePost(arraySlice);
  }, [articleData]);
  return (
    <>
      <Col key={data.id}>
        <Card className="Card--Article shadow-sm">
          <Card.Img
            className="Img__ArticleCard"
            variant="top"
            alt="Card image"
            src={data.img}
          />
          <Card.Body className="Card--BodyArticle">
            {data.category === "PESO UPDATES" ? (
              <div className="div__tag mb-3">
                <span className="tag tag-teal">{data.category}</span>
              </div>
            ) : data.category === "LATEST NEWS" ? (
              <div className="div__tag mb-3">
                <span className="tag tag-purple">{data.category}</span>
              </div>
            ) : data.category === "MUNICIPAL NEWS" ? (
              <div className="div__tag mb-3">
                <span className="tag tag-pink">{data.category}</span>
              </div>
            ) : (
              <></>
            )}
            <Card.Title>{data.title}</Card.Title>
            <Card.Text className="pArticle">{data.description}</Card.Text>
            <div className="div_User">
              <img
                className="Img__CardAuthor"
                // variant="top"
                alt="authimg"
                src={""}
              />
              <div className="div__userInfo">
                <h5 style={{ margin: 0 }}>{data.Author}</h5>
                <small>{data.date_Posted}</small>
              </div>
            </div>
          </Card.Body>
        </Card>
      </Col>
    </>
  );
}

export default ArticleCard;
