import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
// import "./ArticlePosts.scss";
import "./cardArticle.css";
// json file temp for fetching data
import articleData from "./articles.json";

function ArticlePosts() {
  const [articlePost, setArticlePost] = useState([]);
  const [posts, setPosts] = useState([]);

  const fetchData = async () => {
    try {
      const res = await axios
        .get("http://localhost:5000/api/all");

        console.log(res.data)

        const filteredPosts = res.data.filter(post => post.status === 'Published');
          setPosts(filteredPosts)
      
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    
    fetchData();
  }, []);

  return (
    // <Row className="div--ArticleCardContainer">
    //   {posts
    //     .sort((a, b) => new Date(b.date_Posted) - new Date(a.date_Posted))
    //     .slice(0, 3)
    //     .map((data) => (
    //       <Col key={data.id} xs={12} md={3}>
    //         <div className="Card--Article shadow-sm">
    //           <img
    //             className="Img__ArticleCard"
    //             variant="top"
    //             alt="Card image"
    //             src={data.img}
    //           />
    //           <div className="Card--BodyArticle">
    //             {data.category === "PESO UPDATES" ? (
    //               <div className="div__tag mb-3">
    //                 <span className="tag tag-teal">{data.category}</span>
    //               </div>
    //             ) : data.category === "LATEST NEWS" ? (
    //               <div className="div__tag mb-3">
    //                 <span className="tag tag-purple">{data.category}</span>
    //               </div>
    //             ) : data.category === "MUNICIPAL NEWS" ? (
    //               <div className="div__tag mb-3">
    //                 <span className="tag tag-pink">{data.category}</span>
    //               </div>
    //             ) : (
    //               <></>
    //             )}
    //             <h4>{data.title}</h4>
    //             <p className="pArticle">{data.description}</p>
    //             <div className="div_User">
    //               <div className="div__userInfo">
    //                 <h5>{data.Author}</h5>
    //                 <small>{data.date_Posted}</small>
    //               </div>
    //             </div>
    //           </div>
    //           <Link className="lnk-readmore">
    //             Read more <i className="fa-solid fa-arrow-right"></i>
    //           </Link>
    //         </div>
    //       </Col>
    //     ))}
    // </Row>
    <div className="div--ArticleCardContainer">
      {posts
        .sort((a, b) => new Date(b.date_Posted) - new Date(a.date_Posted))
        .slice(0, 3)
        .map((data) => (
          <div key={data.id} className="Card--Article" >
            <img
              className="Img__ArticleCard"
              variant="top"
              alt="Card image"
              src={data.img}
            />
            <div className="Card--BodyArticle">
              {data.category === "PESO UPDATES" ? (
                <div className="div__tag mb-3">
                  <span className="tag tag-teal">{data.category}</span>
                </div>
              ) : data.category === "LATEST NEWS" ? (
                <div className="div__tag mb-3">
                  <span className="tag tag-purple">{data.category}</span>
                </div>
              ) : data.category === "MUNICIPAL NEWS" ? (
                <div className="div__tag mb-3">
                  <span className="tag tag-pink">{data.category}</span>
                </div>
              ) : (
                <></>
              )}
              <h4 className="Article-CardTitle">{data.title}</h4>
              <p className="pArticle">{data.description}</p>
              <div className="div_User">
                <div className="div__userInfo">
                  <h5>{data.Author}</h5>
                  <small>{data.date_Posted}</small>
                </div>
              </div>
            </div>

            <Link className="lnk-readmore">
              Read more <i className="fa-solid fa-arrow-right"></i>
            </Link>
          </div>
        ))}
    </div>
  );
}

export default ArticlePosts;
