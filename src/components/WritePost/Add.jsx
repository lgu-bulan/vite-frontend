import axios from "axios";
import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useNavigate } from "react-router-dom";
import EditorToolbar, { formats, modules } from "./EditorToolbar";
import "./TextEditor.scss";

function Add() {
  let history = useNavigate();
  const [userInfo, setuserInfo] = useState({
    title: "",
    description: "",
    information: "",
  });
  const onChangeValue = (e) => {
    setuserInfo({
      ...userInfo,
      [e.target.name]: e.target.value,
    });
  };
  const ondescription = (value) => {
    setuserInfo({
      ...userInfo,
      description: value,
    });
  };
  const oninformation = (value) => {
    setuserInfo({
      ...userInfo,
      information: value,
    });
  };
  const [isError, setError] = useState(null);
  const addDetails = async (event) => {
    try {
      event.preventDefault();
      event.persist();
      if (userInfo.description.length < 50) {
        setError("Required, Add description minimum length 50 characters");
        return;
      }
      axios
        .post(`http://localhost:8080/addArticle`, {
          title: userInfo.title,
          description: userInfo.description,
          information: userInfo.information,
        })
        .then((res) => {
          if (res.data.success === true) {
            history.push("/");
          }
        });
    } catch (error) {
      throw error;
    }
  };

  return (
    <>
      <div className="App">
        <div className="container-editor">
          <Row xs={1} md={2}>
            <Col md={8}>
              <form onSubmit={addDetails} className="update__forms">
                <h3 className="myaccount-content mb-3"> Write Post </h3>
                <div className="form-row">
                  <div className="form-group col-md-12">
                    <label className="font-weight-bold">
                      {" "}
                      Title <span className="required"> * </span>{" "}
                    </label>
                    <input
                      type="text"
                      name="title"
                      value={userInfo.title}
                      onChange={onChangeValue}
                      className="form-control"
                      placeholder="Title"
                      required
                    />
                  </div>
                  <div className="clearfix"></div>
                  <div className="form-group col-md-12 editor">
                    <label className="font-weight-bold">
                      {" "}
                      Description <span className="required"> * </span>{" "}
                    </label>
                    <EditorToolbar toolbarId={"t1"} />
                    <ReactQuill
                      className="toolbar__overflowY"
                      theme="snow"
                      value={userInfo.description}
                      onChange={ondescription}
                      placeholder={"Write something awesome..."}
                      modules={modules("t1")}
                      formats={formats}
                    />
                  </div>
                  <br />
                  <div className="form-group col-md-12 editor">
                    <label className="font-weight-bold">
                      {" "}
                      Additional Information{" "}
                    </label>
                    <EditorToolbar toolbarId={"t2"} />
                    <ReactQuill
                      className="toolbar__overflowY"
                      theme="snow"
                      value={userInfo.information}
                      onChange={oninformation}
                      placeholder={"Write something awesome..."}
                      modules={modules("t2")}
                      formats={formats}
                    />
                  </div>
                  <br />
                  {isError !== null && (
                    <div className="errors"> {isError} </div>
                  )}
                  <div className="form-group col-sm-12 text-right">
                    <button
                      type="button"
                      className="btn  btn-success btn__theme"
                      variant="success"
                    >
                      {" "}
                      Submit{" "}
                    </button>
                  </div>
                </div>
              </form>
            </Col>

            <Col md={4} style={{ border: "solid green 2px" }}>
              <div
                className="div--editor-side-menu"
                style={{ border: "solid red 2px" }}
              >
                <div className="div--h1-Publish"><h1>Publish</h1></div>
                <div className="div--editor-side-item">
                  <div className="my-4">
                    <span className="side-item-status me-5">
                      <b>Status</b>: Draft
                    </span>
                    <span className="side-item-status my-5">
                      <b>Visibility</b>:Public
                    </span>
                  </div>
                </div>
                <div className="div--btn--editor--side mb-3">
                  <button>Save as Draft</button>
                  <button>Update</button>
                </div>
                <div className="div--radio--category">
                  <h1 className="mb-3">Category</h1>
                  <div className="div__cat">
                    <input
                      type="radio"
                      name="cat"
                      value="Bulan Municipio News"
                      id="MUNICIPAL-NEWS"
                    />
                    <label
                      className="label__cat"
                      htmlFor="Bulan Municipio News"
                    >
                      Bulan Municipio News
                    </label>
                  </div>
                  <div className="div__cat">
                    <input
                      type="radio"
                      name="cat"
                      value="Latest News"
                      id="LATEST-NEWS"
                    />
                    <label className="label__cat" htmlFor="LATEST-NEWS">
                      LATEST NEWS
                    </label>
                  </div>
                  <div className="div__cat">
                    <input
                      type="radio"
                      name="cat"
                      value="PESO Updates"
                      id="PESO-UPDATES"
                    />
                    <label className="label__cat" htmlFor="PESO-UPDATES">
                      PESO UPDATES
                    </label>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}
export default Add;
