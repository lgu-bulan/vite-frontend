import React from "react";
import { Col, Row } from "react-bootstrap";
import tyomeo from "../../Img/mayor/tyomeofinal.jpg";
import Msignature from "../../Img/msignature.png";
import "./mayorInfo.scss";

export const MayorInfo = () => {
  return (
    <div className="main-div-mayorsInfo">
      <div className="div--MayorsInformation">
        <Row xs={1} md={2} className="g-2 ">
          <Col className="content_MI1 d-flex justify-content-center">
            <div className="div-mayor-name">
              <h2>Hon. Mayor Gordola</h2>
            </div>
            <div className="div-img-mayor">
              <img
                className="img-fluid img__Mayor"
                src={tyomeo}
                alt="PicMayor"
              />
            </div>
          </Col>
          <Col className="content_MI2 d-flex flex-column justify-content-center ">
            <h1 className="h1__MITitle">
              <b>22nd Mayor of Bulan</b> re-elected on May 2, 2022
            </h1>
            <p></p>
            <p className="p__MIDescription">
              “Unhan Bulan” encapsulates the vision that Hon. Mayor Romeo “Tyo
              Meo” A. Gordola has for this municipality; the continuous
              advancement of the town of Bulan. Mayor Gordola’s path in public
              service began when he was elected as the Vice Mayor of Bulan on
              2016, subsequently appointed as Mayor in the same year. In his
              first term, Mayor Gordola established the “Botika ng Byan, a
              pharmacy that provides free medicine to the indigent members of
              the community. When he was elected as Mayor on 2019 to which his
              administration pioneered developmental projects, such as the
              construction of farm-to-market roads in far flung barangays and
              rehabilitation of the public market among many others. His
              administration also introduced “Bigkis ng Byan”, a rolling market
              where staple foods are sold at lower than market value. Mayor
              Gordola continues to serve the people of Bulan outside the reals
              of public service through Bulan Lions Club where he is an active
              member.
            </p>
          </Col>
        </Row>
      </div>

      <div className='"div--MayorsMoreInformation'>
        <Row xs={1} md={2} className="g-2 my-4 d-flex flex-direction-column">
          <Col className="content_MI3">
            <div className="div_img_unhan_bayan">
              <img
                className="img-fluid img__FlagshipProgram"
                src="https://res.cloudinary.com/dwfd6pzm5/image/upload/v1687224980/unhan_bulan_0997bed873.jpg"
                alt="UnhanBulanLogo"
              />
            </div>
            <div className="div-signature">
              <h1>MAYOR'S SIGNATURE</h1>
              <img
                className="img-msignature"
                src={Msignature}
                alt="UnhanBulanLogo"
              />
            </div>
          </Col>

          <Col className="content_MI4">
            <div className="div-flagship mx-5 p-2">
              <h1>FLAGSHIP PROGRAM</h1>
            </div>
            <div className="mx-5">
              <p className="d-flex flex-row justify-content-start align-items-center">
                <span className="content_MI4_Fletter d-flex justify-content-center align-items-center">
                  T
                </span>
                <span className="content_MI4_Word">
                  ransformational Governance
                </span>
              </p>
            </div>
            <div className="mx-5">
              <p className="div--MI4--span">
                <span className="content_MI4_Fletter">Y</span>
                <span className="content_MI4_Word">
                  outh and Child Development
                </span>
              </p>
            </div>
            <div className="mx-5">
              <p className="div--MI4--span">
                <span className="content_MI4_Fletter">O</span>
                <span className="content_MI4_Word">rder and Public Safety</span>
              </p>
            </div>
            <div className="mx-5">
              <p className="div--MI4--span">
                <span className="content_MI4_Fletter">M</span>
                <span className="content_MI4_Word">
                  edical and Health Services
                </span>
              </p>
            </div>
            <div className="mx-5">
              <p className="div--MI4--span">
                <span className="content_MI4_Fletter">E</span>
                <span className="content_MI4_Word">
                  nvironmental Protection & Development
                </span>
              </p>
            </div>
            <div className="mx-5">
              <p className="div--MI4--span">
                <span className="content_MI4_Fletter">O</span>
                <span className="content_MI4_Word">
                  rganizational Empowerment
                </span>
              </p>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default MayorInfo;
