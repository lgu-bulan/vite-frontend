import React, {useEffect, useState} from 'react'
import articleData from './articles.json';
import styles from './Menu.module.css';
// import {  } from 'react-bootstrap';
import { Link } from "react-router-dom";

function Menu({cat}) {

    const [posts, setPosts] = useState([])

    
    useEffect(()=>{
        const arrayPosts = articleData.map( data => {
            return(
                // console.log(data);
            <div className={styles['post']} key={data.id}>
                <img src={data.Image_url} alt="" />
                <h2 className={styles["article__title"]}>{data.Title}</h2>
                    <Link className={styles["reacmore__link"]} >Read More  <i className="fas fa-arrow-right"></i></Link>
            </div>
            )
        });

        // Limits the showed result of map array by slicing 0 & 4 range
        let arraySlice = arrayPosts.slice(0, 3);
        setPosts(arraySlice)
    },[articleData]);

    return (
        <div className = {styles["menu"]}>
            <h1 className={styles["menu__title"]}>Other posts you may like</h1>
            {posts}  
        </div>
    )
}

export default Menu