import React from "react";
import { Card } from "react-bootstrap";
import CounterMO from './CounterMO';
import "./MO.scss";
import statsData from "./moData.json";
// const { title, image_url, value } = statsData;

function MunicipalOveriew() {
  // useState for Cards
  // const [statsMO, setStatsMO] = useState([]);
  // useState for counter


  // !!useEffect for cards
  // useEffect(() => {

  //   const arrayStatsMo = 

  //   setStatsMO(arrayStatsMo);
  // }, [statsData]);

  return (
    <div className="div--MunicipalOverview--container">
      <div className="col--MoCards d-flex flex-column flex-sm-row">
      {statsData.map((data) => (
      
        <div key={data.id} className="div--Card--container ">
          <Card className="Card__Mo">
            <Card.Img
              className="Card__ImgMO"
              variant="top"
              src={data.image_url}
              alt="MoCardImg"
            />
            <Card.Body>
              {/* <Card.Title id="stats-count" className="card--MOtitle">
                {data.value}
              </Card.Title> */}
              <CounterMO className="card--MOtitle" key={data.value} value={data.value}/>
              {/* <CounterMO className="card--MOtitle" /> */}
              <Card.Text className="card--MOtext">{data.title}</Card.Text>
            </Card.Body>
          </Card>
        </div>

      ))}

       
      </div>

      <div className="div--MoDescription--container">
       <h1 className="h1__MoTitle">Municipal Overview</h1>
        <p className="p_MoDescription">
          Bulan, officially the Municipality of Bulan, is a 1st class
          municipality in the province of Sorsogon, Philippines. According to
          the 2020 census, it has a population of 105,190 people, making it the
          most populated town in the province. The Municipality of Bulan is
          located at the south-westernmost tip of the Bicol Peninsula of the
          island of Luzon. It has an area of exactly 20,094 hectares and is the
          terminal and burgeoning center of trade and commerce of its
          neighboring towns. It comprises fifty-five (55) barangays and eight
          (8) zones and is populated by people of diversified origin. This
          municipality is bounded on the north by the Municipality of
          Magallanes, on the east by the municipalities of Juban and Irosin, on
          the south by the Municipality of Matnog, and on the west by Ticao
          Pass. It has a distance of 667 kilometres (414 mi) from Manila, 63
          kilometres (39 mi) from the province's capital Sorsogon City, 20
          kilometres (12 mi) from the town of Irosin and 30 kilometres (19 mi)
          from the town of Matnog.
        </p>
      </div>


      <div className="div--Tourism">
          <div className="header_tourism">
              <h1>Discover Our Lovely and Vibrant Places!</h1>
              <p>Experience the Tradition, Great and the Natural Beauty of the City</p>
              <button>Explore Town</button>
          </div>
      </div>
    </div>
  );
}

export default MunicipalOveriew;
