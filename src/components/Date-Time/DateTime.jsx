import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import "./DateTime.scss";

function DateTime() {
  const [dateTime, setDateTime] = useState(new Date());

  useEffect(() => {
    const intervalId = setInterval(() => {
      setDateTime(new Date());
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  const day = dateTime.toLocaleDateString(undefined, { weekday: "long" });
  const time = dateTime.toLocaleTimeString();

  return (
    <div className="dateTimeBody">
      <div className="container-dateTimeBody">
        <Row xs={2} md={2} className="banner-row">
          <Col xs={3} md={6} className="col-lgu-bulan-logo">
            <Row xs={2} md={2} className="banner_inner">
              <Col xs={3} md={3} className="col1_inner">
                <img
                  src="https://res.cloudinary.com/dylqar8xg/image/upload/v1687154254/small_LGU_MUNICIPAL_21a6ea2409.png"
                  alt="bulanlogo"
                />
              </Col>
              <Col xs={9} md={9} className="col2_inner">
                <h1>BULAN-4706</h1>
                <p className="underline-banner"></p>
                <h4>Home of the Padaraw Festival</h4>
              </Col>
            </Row>
          </Col>
          <Col xs={4} md={8}></Col>
        </Row>
      </div>
    </div>
  );
}

export default DateTime;
{
  /* <Row xs={1} sm={1} md={2} className="banner-row">
<Col xs={12} md={6} className="col-lgu-bulan-logo">
  <div className="wrapper-lgu-bulan  mx-3">
    <Row xs={2} md={2} className="banner_inner_row">
      <Col xs={4} md={4} className="col_lgu_bulan_logo">
        <img
          className="lgu_bulan_logo mx-1"
          src="https://res.cloudinary.com/dylqar8xg/image/upload/v1687154254/small_LGU_MUNICIPAL_21a6ea2409.png"
          // margin="auto"

          // className="d-inline-block align-top"
          alt="LGU BULAN LOGO"
        />
      </Col>
      <Col xs={8} md={8} className="col_lgu_bulan_name">
        <h1>Bulan 4706</h1>
        <div className="underline-banner"></div>
        <h3>Home of the Padaraw Festival!</h3>
      </Col>
    </Row>
  </div>
</Col>
{/* <Col xs={12} md={6} className="col-date-time">
  <div className="container-date-time">
    <span className="dateTime">
      {day} {time}
    </span>
    <br></br>
    <p className="p_OfficeHrs">Office Hours WEEKDAYS 8 AM - 5 PM</p>
  </div>
</Col> */
}
// </Row> */}
