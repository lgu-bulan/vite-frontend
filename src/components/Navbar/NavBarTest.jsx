import React from "react";
import { Nav, NavDropdown, Navbar, Offcanvas } from "react-bootstrap";
import DateTime from "../Date-Time/DateTime";
import { Link } from "react-router-dom";
// import "./navBarTest.scss";
import "./test.scss";

function CollapsibleExample() {
  return (
    <>
      <DateTime />
      <Navbar
        collapseOnSelect
        expand="md"
        className="Main-Navbar justify-content-end"
      >
        <Navbar.Toggle
          className="me-3 "
          aria-controls="offcanvasNavbar-expand-sm"
        />
        <Navbar.Offcanvas
          className="main-navbar-offcanvas"
          id="offcanvasNavbar-expand-sm"
          aria-labelledby="offcanvasNavbarLabel-expand-sm"
          placement="end"
          // className="justify-content-end"
        >
          <Nav className="me-2 navbar-nav-offcanvass ms-3">
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id="offcanvasNavbarLabel-expand-sm">
                Bulan-4706
              </Offcanvas.Title>
            </Offcanvas.Header>

            <Offcanvas.Body className="nav-body">
              <Nav.Link className="Main-Navbar-link px-2" href="/">
                Home
              </Nav.Link>

              {/* NEsted DropDOwn ABOUT BULAN Start*/}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="About Bulan"
                id="navbarScrollingDropdown"
                renderMenuOnMount={true} // cause shpw dropdown on render
              >
                <NavDropdown.Item className="Main__NavDropdown__Item"  href="/Overview">
                  {/* <Link to="/Overview">Overview</Link> */}
                  Overview
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <div className="nav__aboutBulan__sub">
                  <NavDropdown
                    drop="end"
                    title="History"
                    id="navbarScrollingDropdown"
                    className="main-sub-menu"
                    renderMenuOnMount={true}
                    // style={{ padding: "0 ", margin: "0" }}
                  >
                    <NavDropdown.Item href="/TownHistory">
                      Town
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/BarangayHistory">
                      Barangays
                    </NavDropdown.Item>
                  </NavDropdown>
                </div>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.3">Tourism</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Business</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  Town Gallery
                </NavDropdown.Item>
              </NavDropdown>
              {/* NEsted DropDOwn ABOUT BULAN END*/}

              {/* NEsted DropDOwn OUR LGU Start*/}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="Our LGU"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                <NavDropdown.Item href="#action/3.1">
                  Mission, Vision and <br></br>Quality Policy
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <div className="nav__OurLgu__sub__officeofthemayor">
                  {/* office of the mayor sub */}
                  <NavDropdown
                    drop="end"
                    title="Office of the Mayor"
                    id="collasible-nav-dropdown"
                    className="custom-dropdown main-sub-menu"
                    renderMenuOnMount={true}
                    // style={{ padding: "0 16px", margin: "0" }}
                  >
                    <NavDropdown.Item href="#action/3.1">
                      Mayor's Profile
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.2">
                      Executive Orders
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown
                      drop="end"
                      title="Executive & Legislative Agenda"
                      id="collasible-nav-dropdown"
                      className="custom-dropdown main-sub-menu"
                      renderMenuOnMount={true}

                      // style={{ padding: "4px 16px"}}
                    >
                      <NavDropdown.Item href="#action/3.1">
                        Comprehensive Land
                        <br />
                        and Use Plan
                      </NavDropdown.Item>
                    </NavDropdown>
                  </NavDropdown>
                  {/* office of the mayor sub end */}
                </div>

                <div className="nav__OurLgu__sub__officeofthemayor__executiveagenda">
                  {/* Sanguniang Bayan sub */}
                  <NavDropdown.Divider />
                  <NavDropdown
                    drop="end"
                    title="Sanguniang Bayan"
                    id="collasible-nav-dropdown"
                    className="custom-dropdown main-sub-menu"
                    renderMenuOnMount={true}
                    // style={{ color: "" }}
                  >
                    <NavDropdown.Item href="#action/3.1">
                      Members
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.2">
                      Resolutions
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.1">
                      Ordinance
                    </NavDropdown.Item>
                  </NavDropdown>
                  {/* Sanguniang Bayan sub  END*/}
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.1">
                    Government Services
                  </NavDropdown.Item>
                </div>

                {/* <NavDropdown.Divider /> */}
              </NavDropdown>

              {/* NEsted DropDOwn OUR LGU END*/}

              {/*  News & Publications DropDown Start*/}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="News & Publications"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                <NavDropdown.Item href="#action/3.1">News</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.2">
                  Upcoming Events
                </NavDropdown.Item>
              </NavDropdown>
              {/*  News & Publications DropDown End*/}

              {/*Transparency DropDOwn Start */}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="Transparency"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                {/* Bids & Awards Nested DropDown Start */}
                <div className="nav__Transparency__BidsAndAwards">
                  {/* Sanguniang Bayan sub */}
                  <NavDropdown
                    drop="end"
                    title="Bids and Awards"
                    id="collasible-nav-dropdown"
                    className="custom-dropdown main-sub-menu"
                    renderMenuOnMount={true}
                    style={{ color: "black" }}
                  >
                    <NavDropdown.Item href="/Transparency/InvitationToBid">
                      Invitation to Bid
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/Transparency/RequestForQuotation">
                      Request for Quotation
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/Transparency/NoticeToProceed">
                      Notice to Proceed
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/Transparency/NoticeOfAwards">
                      Notice of Awards
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/Transparency/NoticeOfNegotiatedProcurement">
                      Notice of Negotiated <br></br> Procurement
                    </NavDropdown.Item>
                  </NavDropdown>
                  <NavDropdown.Divider />
                  {/* Sanguniang Bayan sub  END*/}
                  <NavDropdown.Item href="#action/3.2">
                    Annual Report
                  </NavDropdown.Item>
                </div>
                {/* Bids & Awards Nested DropDown End */}
              </NavDropdown>
              {/* Transparency DropDOwn End  */}

              {/* E-Services Start  */}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="e-Services"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                <NavDropdown.Item href="#action/3.3">
                  E-Community
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="https://prod9.ebpls.com/bulansorsogon/index.php/login">
                  iBPLS (Integrated <br /> Business Processing <br />
                  and Licensing System)
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="https://pnpclearance.ph/">
                  Police Clearance
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.3">
                  Downloadables
                </NavDropdown.Item>
              </NavDropdown>
              {/* E-Services End  */}
              <Nav.Link className="Main-Navbar-link px-2" href="/login">
                LogIn
              </Nav.Link>
              {/* <InputGroup className="mb-3" style={{ fontSize: "12px" }}>
                <Form.Control
                  placeholder="Recipient's username"
                  aria-label="Recipient's username"
                  aria-describedby="basic-addon2"
                />
                <Button variant="outline-secondary" id="button-addon2">
                  <i
                    className="fa-solid fa-magnifying-glass me-2"
                    style={{ fontSize: "2.005rem", color: "#fefefe" }}
                  ></i>
                </Button>
              </InputGroup> */}
            </Offcanvas.Body>
          </Nav>
        </Navbar.Offcanvas>
      </Navbar>
    </>
  );
}

export default CollapsibleExample;
