import React, { useEffect, useState } from 'react';
import { Card, Col, Image, Row } from 'react-bootstrap';
import "./govServices.css";
import dataService from "./gsData.json";

function GovServices() {
  
  const [listService, setListService] = useState([])
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    const arrayService = dataService.map(data => {
      return (
        <Col key={data.id}>
          <Card key={data.id} id="Card--Gov--Service" className='shadow-sm'>
            <div>
              <Row  xs={2} md={2} className='g-2 m-2' >
                <Col id="colImage" xs={4} md={4} className='px-2' >
                    <Image id="cardImage" variant="top" src= {data.image_url}/>
                </Col>
  
                <Col xs={8} md={8}>
                  <Card.Body id='gov--service--card--body' className='text-center mx-2 px-2'>
                    <Card.Title className='CardTitle-GovService'>{data.service_name}</Card.Title>
                  </Card.Body>
                </Col>
              </Row>
            </div>
          </Card>
        </Col>
      )
    });
    setListService(arrayService)
  }, [dataService]);

  return (

  
    <div className='div_container_govService'>
      <div className='gov--service--header d-sm-flex justify-content-center text-center'>
        <Row xs={1} md={3} className="g-2 my-2 row__govService">
            <Col className='gov__service__header__title p-4 m-0'>
              <h2>Government Services</h2>
            </Col>
            <Col className='gov__service__header__description p-3 m-0'>
              <p>LGU – Bulan is mandated by law to provide basic services that are responsive to the needs of their citizens.</p>
            </Col>
            <Col className='gov__service__header__button p-4 m-0'>
              <button id="gov_service_header_button" className='btn shadow-sm' type="button" >View More</button>
            </Col>
        </Row>
      </div>
  
      <div className='div--govServiceCards'>
        <Row id="horizontalCardRow" xs={1} md={3} className="g-3 py-5">
        {listService}
        </Row>
      </div>
    </div>


  )
}

export default GovServices