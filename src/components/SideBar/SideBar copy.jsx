import React, { useEffect, useState, useRef } from "react";
import "./Sidebar.scss";
import { NavLink } from 'react-router-dom';
// import "./Sidebar.css";
// import DashBoard from "./DashBoard";
import profpic from '../../Img/profile-picture.png';
import Switch from "react-switch";
// SIDEBAR
function SideBar() {
  const [mode, setMode] = useState(
    localStorage.getItem("mode") === "dark" ? "dark" : "light"
  );


  const bodyRef = useRef(null);

  useEffect(() => {
    bodyRef.current.classList.toggle("dark", mode === "dark");
    localStorage.setItem("mode", mode);
  }, [mode]);

  const handleModeToggle = () => {
    setMode(mode === "light" ? "dark" : "light");
  };

//   dashboard
const [status, setStatus] = useState(
  () => localStorage.getItem("status") || "open"
);

const sidebarRef = useRef(null);

useEffect(() => {
  sidebarRef.current.classList.toggle("close", status === "close");
  console.log(status)
  localStorage.setItem("status", status);
}, [status]);

const handleSidebarToggle = () => {
  setStatus(status === "open" ? "close" : "open");
};


const menuItem=[
  {
      path:"/",
      name:"Dashboard",
      icon:<i className="fa-solid fa-house sidebar-icon"></i>
  },
  {
      path:"/about",
      name:"Content",
      icon:<i className="fa-solid fa-pen-to-square sidebar-icon"></i>
  },
  {
      path:"/analytics",
      name:"Asset",
      icon:<i className="fa-solid fa-link sidebar-icon"></i>
  },
 
]
  return (
    <div id='body-div' ref={bodyRef}>
      <div className="my-admin-dashboard">
        <nav className="Sidebar--Nav" ref={sidebarRef}>
          <div className="div_Sidebar_Nav_Brand">
            <div className="div_Sidebar_image">
              <img
                src="https://bulan4706.com/wp-content/uploads/2022/10/lgo.png"
                alt="bulan4706Logo"
              />
            </div>

            <span className="span_Sidebar_logo_name">Bulan-4706</span>
          </div>

          <div className="Sidebar--menu-items">
            <ul className="Sidebar--nav-links">
              <li>
                <a href="/">
                  <i className="fa-solid fa-house sidebar-icon"></i>
                  <span className="link-name">Dahsboard</span>
                </a>
              </li>
              <li>
                <a href="/">
                  <i className="fa-solid fa-pen-to-square sidebar-icon"></i>
                  <span className="link-name">Content</span>
                </a>
              </li>
              <li>
                <a href="/">
                  {/* <i className="fa-solid fa-images"></i> */}
                  <i className="fa-solid fa-link sidebar-icon"></i>
                  <span className="link-name">Asset</span>
                </a>
              </li>

              <li>
                <a href="/">
                  <i className="fa-solid fa-share sidebar-icon"></i>
                  <span className="link-name">Share</span>
                </a>
              </li>
            </ul>

            <ul className="logout-mode">
              <li>
                <a href="/">
                  <i className="fa-solid fa-right-from-bracket sidebar-icon"></i>
                  <span className="link-name">Logout</span>
                </a>
              </li>

              <li className="mode">
                <a href="/">
                  <i className="fa-solid fa-moon sidebar-icon"></i>
                  <span className="link-name">Dark Mode</span>
                </a>

                <div className="mode-toggle">
                  <span
                    className="switch"
                    checked={mode === "dark"}
                    onClick={handleModeToggle}
                  ></span>
                  {/* <Switch  className="switch-mode" checked={mode === "dark"} onChange={handleModeToggle}></Switch> */}
                </div>
              </li>
            </ul>
          </div>
        </nav>
                  {/* !!DASHBOARD */}
        <section className="dashboard">
        <div className="top">
          <i className="fa-solid fa-bars-staggered sidebar-toggle" onClick={handleSidebarToggle}></i>

          <div className="search-box">
            <i className="fa-solid fa-magnifying-glass"></i>
            <input type="text" placeholder="Search here..." />
          </div>
          <i className="fa-solid fa-bell"></i>
          <span className="span-currentuser">MARK ELVIN SIMORA</span>
          <img
            src={profpic}
            alt="crruserImg"
          />
        </div>

        <div className="dash-content">
          <div className="overview">
            <div className="title">
              <i className="fa-solid fa-tachograph-digital"></i>
              <span className="text">Dashboard</span>
            </div>

            <div className="boxes">
              <div className="box box1">
                <i className="uil uil-thumbs-up"></i>
                <span className="text">Total Post</span>
                <span className="number">50,120</span>
              </div>
              <div className="box box2">
                <i className="uil uil-comments"></i>
                <span className="text">Comments</span>
                <span className="number">20,120</span>
              </div>
              <div className="box box3">
                <i className="uil uil-share"></i>
                <span className="text">Total Share</span>
                <span className="number">10,120</span>
              </div>
            </div>
          </div>

          <div className="activity">
            <div className="title">
              <i className="fa-solid fa-clock"></i>
              <span className="text">Recent Activity</span>
            </div>

            <div className="table-responsive">
              <table className="table table-secondary  table-striped">
                <thead>
                  <tr>
                    <th scope="col">Acc. Name</th>
                    <th scope="col">Acc. Type</th>
                    <th scope="col">Date</th>
                    <th scope="col">Activity</th>
                  </tr>
                </thead>
                <tbody className="table-group-divider">
                  <tr>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td>Liked</td>
                  </tr>
                  <tr>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    <td>Liked</td>
                  </tr>
                  <tr>
                    <td colSpan="2">Larry the Bird</td>
                    <td>@twitter</td>
                    <td>Liked</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      </div>
    </div>
  );
}

export default SideBar;
