import React from "react";
import { Col, Row } from "react-bootstrap";
import "./pre-footer.css";
import Footer from "./Footer";
import {
  FaBuildingColumns,
  FaRegEnvelopeOpen,
  FaPhoneVolume,
} from "react-icons/fa6";

function prefooter() {
  return (
    <>
      <div className="div--Prefooter">
        <Row xs={1} md={3}>
          <Col>
            <div className="div-prefooter-logo">
              <img
                className="img-fluid img__prefooterlogo p-3"
                src="https://res.cloudinary.com/dwfd6pzm5/image/upload/v1687225477/transparency_seal_resized_6b2176e78c.png"
                alt="Transparency Logo"
              />
            </div>
          </Col>

          <Col xs={12}>
            <div className="div__PrefooterContact">
              <h4 className="mb-3 text-center ps-5">Contact Info</h4>

              <div className=" text-center mb-4">
                <Row
                  xs={1}
                  md={2}
                  className="justify-content-center align-items-center"
                >
                  <Col xs={2} md={2}>
                    <FaBuildingColumns className="prefooterIcon-1" />
                  </Col>
                  <Col className="p-3" xs={10} md={10}>
                    <h5 className="my-0 ftr-itm-title">
                      Visit The Municipal Hall
                    </h5>
                    <p className="my-0">Brgy. Aquino BUlan, Sorsogon</p>
                  </Col>
                </Row>
              </div>

              <div className=" text-center mb-5">
                <Row
                  xs={1}
                  md={2}
                  className="justify-content-center align-items-center"
                >
                  <Col
                    xs={2}
                    md={2}
                    className="justify-content-center align-items-center"
                  >
                    <FaRegEnvelopeOpen className="prefooterIcon-1" />
                  </Col>
                  <Col
                    xs={10}
                    md={10}
                    className="justify-content-center align-items-center"
                  >
                    <h5 className="m-0 px-3 ftr-itm-title">Email</h5>
                    <p className="my-0 px-3 ftr-itm-dsc">lgubulan@gmail.com</p>
                  </Col>
                </Row>
              </div>

              <div className="text-center mb-1">
                <Row
                  xs={1}
                  md={2}
                  className="justify-content-center align-items-center"
                >
                  <Col xs={2} sm={2} md={2}>
                    <FaPhoneVolume className="prefooterIcon-1" />
                  </Col>
                  <Col
                    xs={10}
                    md={10}
                    className="justify-content-center align-items-cent"
                  >
                    <h5 className="my-0 px-3 ftr-itm-title">Phone</h5>
                    <p className="my-0 px-3 ftr-itm-dsc">(123)456-7890</p>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>

          <Col xs={12}>
            <div className="div__PreFooterPhoneNumbers">
              <h4 className="mb-3 text-center ps-5">Phone Numbers</h4>
              <div className=" text-center mb-3">
                <Row
                  xs={1}
                  md={2}
                  className="justify-content-center align-items-center"
                >
                  <Col xs={2} md={2}>
                    <img
                      src="https://res.cloudinary.com/dwfd6pzm5/image/upload/v1687225414/V968x_U_Uj_400x400_e89c032a05.png"
                      alt="MDRRMO"
                      className="departmentPhoneIcons"
                    />
                  </Col>
                  <Col xs={10} md={10}>
                    <h5>MDRRMO/Rescue</h5>
                    <p>09618234132</p>
                  </Col>
                </Row>
              </div>

              <div className=" text-center mb-3">
                <Row
                  xs={1}
                  md={2}
                  className="justify-content-center align-items-center"
                >
                  <Col xs={2} md={2}>
                    <img
                      src="https://res.cloudinary.com/dwfd6pzm5/image/upload/v1687225243/PNP_f13f1e3def.png"
                      alt="PNP"
                      className="departmentPhoneIcons"
                    />
                  </Col>
                  <Col xs={10} md={10}>
                    <h5>PNP</h5>
                    <p>09198292101</p>
                  </Col>
                </Row>
              </div>

              <div className=" text-center mb-3">
                <Row
                  xs={1}
                  md={2}
                  className="justify-content-center align-items-center"
                >
                  <Col xs={2} md={2}>
                    <img
                      src="https://res.cloudinary.com/dwfd6pzm5/image/upload/v1687225091/BFP_4c77273a45.png"
                      alt="BFP"
                      className="departmentPhoneIcons"
                    />
                  </Col>
                  <Col xs={10} md={10}>
                    <h5>BFP</h5>
                    <p>09301382814</p>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <Footer />
    </>
  );
}

export default prefooter;
