import React, { useEffect } from 'react';
import { Button, Card, Col, Container, Form, FormControl, Row } from 'react-bootstrap';
import { Link, } from 'react-router-dom';
import ButtonTop from "../GtoTop/ButtonTop";
import Menu from '../Menu/Menu';
import "./SingleArticleView.scss";
import articleData from "./articles.json";
// import { solid, regular, brands, icon } from '@fortawesome/free-solid-svg-icons'


function SingleArticleView() {

    useEffect(() => {
        // set the background color of the body element
        document.body.style.backgroundColor = '#f0f5ec';
        // document.body.style.marginBottom = '50px';
        return () => {
            document.body.style.backgroundColor = null;
            document.body.style.marginBottom = null;
        };
    }, []);

    return (

        <Container>
            <div className ="singleArticleContainer">
                <Row xs={1} md={2} className="g-4">
                {/* 1st column */}
                    <Col className='articleView' sm={12} md={8}>
                        <Card>
                            <div className="single">
                                <div className="content">
                                    <img className="Img__Header" style={{padding: 0, borderRadius: "3px"}} src='https://www.thoughtco.com/thmb/cAvGc4QBGDdGWTuxkbkhOyx2UHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/166370483-56cb36da5f9b5879cc54103c-5c54ad6b46e0fb00013a2205.jpg' alt="try-img" />

                                    <div className="userSingleView">
                                        <img
                                            id='cardImgAuthor'
                                            src='https://scontent.fmnl13-1.fna.fbcdn.net/v/t1.6435-9/83185366_907843982946130_2332233001972269056_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeEf-ksbWpIAit2p-gtUEujQhsUtNGZER4KGxS00ZkRHgvMnTaVBUDVDj82c6q2A_HUavq1UFs5Ro27LpWpTmKUb&_nc_ohc=fR808a1-4EcAX9Xqaw7&_nc_ht=scontent.fmnl13-1.fna&oh=00_AfAFH7EPh5RtLJHv7nhanPEFtQT-dlYGxEdnqqiVCmQ27g&oe=64140C86'
                                            alt="user-img"
                                        />
                                        <div className="info">
                                            <h5>Lorem Ipsum</h5>
                                            <p>Date Posted Here</p>
                                        </div>
                                        <div className="edit">
                                            <Link to={`/`} state=''>
                                                <img src='' alt="" />
                                            </Link>
                                            <img onClick='' src='' alt="" />
                                        </div>
                                        {/* {currentUser.username === post.username && (
                                            <div className="edit">
                                                <Link to={`/write?edit=2`} state={post}>
                                                    <img src={Edit} alt="" />
                                                </Link>
                                                <img onClick={handleDelete} src={Delete} alt="" />
                                            </div>
                                        )} */}
                                    </div>

                                    <div className="articleBody">
                                        <h1 className="article__Title">Aute aute proident quis nulla elit aliquip in et dolore aliquip adipisicing nulla.</h1>
                                        <p className="article__Contents">Incididunt voluptate ut sit eu nostrud incididunt incididunt veniam ex. Aliqua elit mollit in incididunt Lorem qui ex. Tempor est aliquip nisi est exercitation nostrud adipisicing cillum aliqua et et in. Cillum ex sunt pariatur ipsum cupidatat incididunt laboris amet irure non Lorem. Cupidatat pariatur tempor mollit nisi ut voluptate in dolore amet nulla reprehenderit. Et mollit adipisicing ut velit labore sint do nostrud quis officia.</p>
                                        <br></br>
                                        <p className="article__Contents">Incididunt voluptate ut sit eu nostrud incididunt incididunt veniam ex. Aliqua elit mollit in incididunt Lorem qui ex. Tempor est aliquip nisi est exercitation nostrud adipisicing cillum aliqua et et in. Cillum ex sunt pariatur ipsum cupidatat incididunt laboris amet irure non Lorem. Cupidatat pariatur tempor mollit nisi ut voluptate in dolore amet nulla reprehenderit. Et mollit adipisicing ut velit labore sint do nostrud quis officia.</p>
                                    </div>                                         
                                </div>
                                    
                            </div>

                        </Card>
                    </Col>
                    
                {/* 2nd column */}
                    <Col className='category' sm={12}  md={4}>
                        <Form className="d-flex searchForm pb-4">
                            <FormControl
                                type='search'
                                placeholder='Search'
                                className='me-2'
                                aria-label='Search'
                            />
                            <Button variant='outline-success'>
                                <i className="fa-solid fa-magnifying-glass"></i>
                            </Button>
                        </Form>
                        <Card>
                            <div className='containerCategory'>
                                <h2 className='category__title py-2'>Category</h2>
                                {/* <p>Hello, <FontAwesomeIcon icon={faUser} />!</p> */}
                                <ul style={{padding: '0 15px 0 15px', marginBottom: '30px'}}>
                                    <li className='category__list'><i className="fa-regular fa-star"></i>Bulan Municipio News</li>
                                    <li className='category__list'><i className="fa-regular fa-star"></i>Latest News</li>
                                    <li className='category__list'><i className="fa-regular fa-star"></i>PESO Updates</li>
                                </ul>
                            </div>
                            {/* cat is a prop from Menu Component */}
                            {/* <Menu cat={articleData} /> */}
                        </Card>

                        <Card className='my-2'>
                            <Menu cat={articleData} />
                        </Card>
                    </Col>
                </Row>
            </div>
            <ButtonTop />
        </Container>
    )
}

export default SingleArticleView