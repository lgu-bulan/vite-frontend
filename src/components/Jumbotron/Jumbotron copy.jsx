import React, { useState, useEffect } from 'react'
import "./jumbotron.scss"
import { Carousel, Button } from "react-bootstrap";
import { Spin } from 'antd';
// import coastal from "../../Img/Bulan-Tourism/CoastalWater-cln.jpg"
// import park from "../../Img/Bulan-Tourism/rizal park2.jpg"
// import padaraw from "../../Img/Bulan-Tourism/Padaraw1.jpg"

function Jumbotron() {

  const [loaded, setLoaded] = useState(false);


  useEffect(() => {
    console.log(`check Loaded txt animation ${loaded}`)
    setLoaded(true);
  }, []);

  return (
    <div>
      <Carousel fade className='carousel-home'>
        <Carousel.Item >
          <div className='carousel-container'>
            <span className={`crsl-spn1 ${loaded ? "loaded" : ""}`}><b>Endowed </b>with </span>
            <span className={`crsl-spn2 ${loaded ? "loaded" : ""}`}><b>Natural</b> resources</span>
            <span className={`crsl-p ${loaded ? "loaded" : ""}`}>and wonderful sceneries.</span>
            <button className={`crs1-btn ${loaded ? "loaded2" : ""}`}>Explore!</button>
          </div >
          <Carousel.Caption>
          </Carousel.Caption>

        </Carousel.Item>

        {/* <Carousel.Item>
        <img
          className="d-block "
          src={park}
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item> */}
        {/*
      <Carousel.Item>
        <img
          className="d-block "
          src={padaraw}
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block "
          src={padaraw}
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>4th slide label</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        </Carousel.Caption>
      </Carousel.Item> */}
      </Carousel>
    </div>

  )
}

export default Jumbotron

