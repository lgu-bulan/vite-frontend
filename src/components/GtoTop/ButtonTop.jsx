import React, { useEffect, useState } from "react";
import "./buttonTop.css";
import { VerticalAlignTopOutlined } from "@ant-design/icons";

// import "@fortawesome/fontawesome-free/css/all.min.css";
const GoToTopButton = () => {
  const [showButton, setShowButton] = useState(false);

  useEffect(() => {
    document.addEventListener("scroll", () => {
      setShowButton(window.scrollY > 100);
    });
  }, []);

  useEffect(() => {
    const handleScrollButtonVisibility = () => {
      window.pageYOffset > 300 ? setShowButton(true) : setShowButton(false);
    };

    window.addEventListener("scroll", handleScrollButtonVisibility);

    return () => {
      window.removeEventListener("scroll", handleScrollButtonVisibility);
    };
  }, []);

  const handleScrollToTop = () => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  };

  return (
    <>
      {/* if showButton is true show div-button */}
      {showButton && (
        <div>
          <button className="button__gotoTop" onClick={handleScrollToTop}>
            <VerticalAlignTopOutlined className="gototop_icon"/>
          </button>
        </div>
      )}
    </>
  );
};

export default GoToTopButton;
