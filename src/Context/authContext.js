import axios from "axios";
import { createContext, useEffect, useState } from "react";
export const AuthContext = createContext()

export const AuthContexProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem("user") || null))
    const [isAuthenticated, setIsAuthenticated] = useState(JSON.parse(localStorage.getItem("AuthenticationStatus") || false));


    const login = async (inputs) => {

        // const response =  await axios.post("http://localhost:5000/api/auth/LogIn", inputs)
        //     setCurrentUser(response.data)
        //     // console.log('\x1b[36m', response)
        //     // console.log('\x1b[36m', response.data)
        //     console.log('successful')
        //     return response;

        try {
            const response = await axios.post("http://localhost:5000/api/auth/LogIn", inputs);
            setCurrentUser(response.data);
            setIsAuthenticated(true);
            console.log(response);
            console.log(response.data);
            console.log('successful');
            console.log(isAuthenticated) 
            return response;
        } catch (error) {
            console.error(error);
            throw new Error('Login failed');
        }
    };


    const unsetUser = () => {
        localStorage.clear()
        setCurrentUser(null)
    }

    // useEffect(() => {
    //     //set the user state back to it's original value
    //     setCurrentUser({ accessToken: null })
    // }, [])

    const logout = async (inputs) => {
        const res = await axios.post("/auth/logout");
        setCurrentUser(null);
    }


    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(currentUser));
        localStorage.setItem("AuthenticationStatus", JSON.stringify(isAuthenticated));
        // console.log(isAuthenticated)
        // const token = localStorage.getItem('user');
        // if (token !== null || token === '') {
        //   setIsAuthenticated(true);
        // }
    }, [currentUser,isAuthenticated]);

    return (
        <AuthContext.Provider value={{ currentUser, isAuthenticated, setIsAuthenticated, login, logout, unsetUser }}>
            {children}
        </AuthContext.Provider>
    )

}   